from __future__ import annotations

from manul.connection import get_connection
from manul.connection import get_engine
from sqlalchemy.engine import Connection
from sqlalchemy.engine import Engine


profile = {
    'host': '127.0.0.1',
    'port': 3306,
    'user': 'root',
    'password': 'root',
    'database': 'dwh',
}


def test_get_engine():
    engine = get_engine(profile)
    assert isinstance(engine, Engine)
    assert str(engine.url) == 'mysql+mysqldb://root:root@127.0.0.1:3306/dwh?charset=utf8mb4'


def test_get_connection():
    conn: Connection = None
    try:
        conn = get_connection(profile)
        assert isinstance(conn, Connection)
    except Exception:
        assert conn is None
