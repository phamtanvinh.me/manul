from __future__ import annotations

import petl
from manul.util import misc


def test_to_date_key():
    """Test to_date_key."""
    assert misc.to_date_key('2019-01-01') == '20190101'
