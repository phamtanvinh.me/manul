from __future__ import annotations

from ast import literal_eval

from deepdiff import DeepDiff
from manul.order_events.etl import etl_event_order
from manul.order_events.etl import etl_order_was_created
from manul.order_events.etl import etl_order_was_overdue
from manul.order_events.models import OrderEvents
from manul.order_events.models import OrderWasCreated
from manul.order_events.models import OrderWasOverdue


def test_etl_order_was_overdue(maxwell_order_was_overdue, dwh_order_was_overdue: dict):
    """Test etl_order_was_overdue."""
    order_events_data = maxwell_order_was_overdue.data
    order_events = OrderEvents(**order_events_data)
    result = etl_event_order(order_events)

    assert (
        DeepDiff(result['fact_order_event'], dwh_order_was_overdue['fact_order_event'])
        == {}
    )

    order_was_overdue_data = {
        'order_event_id': order_events_data['id'],
        'event_id': order_events_data['event_id'],
        **literal_eval(order_events_data['payload']),
    }
    order_was_overdue = OrderWasOverdue(**order_was_overdue_data)

    result = etl_order_was_overdue(order_was_overdue)
    assert (
        DeepDiff(
            result['fact_order_overdue'], dwh_order_was_overdue['fact_order_overdue'],
        )
        == {}
    )


def test_etl_order_was_created(maxwell_order_was_created, dwh_order_was_created: dict):
    """Test etl_order_was_created."""

    order_events_data = maxwell_order_was_created.data
    order_events = OrderEvents(**order_events_data)

    result = etl_event_order(order_events)
    assert (
        DeepDiff(result['fact_order_event'], dwh_order_was_created['fact_order_event'])
        == {}
    )

    order_was_created_data = {
        'order_event_id': order_events_data['id'],
        'event_id': order_events_data['event_id'],
        **order_events_data['payload'],
    }
    order_was_created = OrderWasCreated(**order_was_created_data)
    result = etl_order_was_created(order_was_created)

    for table_name in ('dim_order_item', 'fact_order_item'):
        diff = DeepDiff(result[table_name], dwh_order_was_created[table_name])
        assert diff == {}
