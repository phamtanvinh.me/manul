from __future__ import annotations

from manul.orders.etl import etl_orders
from manul.orders.models import Orders


def test_etl_orders(maxwell_order, dwh_orders: dict):
    """Test etl_orders."""

    data = maxwell_order.data
    order = Orders(**data)
    result = etl_orders(order)

    assert result == dwh_orders
