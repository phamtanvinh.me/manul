from __future__ import annotations

from manul.orders.models import Orders


def test_maxwell_dummy(maxwell_dummy):
    maxwell_dummy.data = 'dummy'
    assert maxwell_dummy.convert_data(Orders) is None
