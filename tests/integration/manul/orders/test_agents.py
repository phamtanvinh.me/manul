from __future__ import annotations

import pytest
from manul.orders.agents import orders_agent


@pytest.mark.asyncio()
async def test_orders_agent(test_app, maxwell_order, dwh_orders: dict):
    """Test Maxwell agent."""

    async with orders_agent.test_context() as agent:
        event = await agent.put(maxwell_order)
        result = agent.results[event.message.offset]

        assert result == dwh_orders
