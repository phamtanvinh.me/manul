from __future__ import annotations

import pytest
from deepdiff import DeepDiff
from manul.order_events.agents import order_was_created_agent
from manul.order_events.models import OrderWasCreated


@pytest.mark.asyncio()
async def test_order_was_created_agent(
    test_app, maxwell_order_was_created, dwh_order_was_created,
):
    async with order_was_created_agent.test_context() as agent:
        order_events_data = maxwell_order_was_created.data

        data = {
            'order_event_id': order_events_data['id'],
            'event_id': order_events_data['event_id'],
            **order_events_data['payload'],
        }

        event = await agent.put(OrderWasCreated(**data))
        result = agent.results[event.message.offset]

        for table_name in ('dim_order_item', 'fact_order_item'):
            diff = DeepDiff(result[table_name], dwh_order_was_created[table_name])
            assert diff == {}
