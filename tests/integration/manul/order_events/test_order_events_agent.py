from __future__ import annotations

from unittest.mock import Mock
from unittest.mock import patch

import pytest
from manul.order_events.agents import dummy_agent
from manul.order_events.agents import order_events_agent


dummy_payload = {
    'order_event_id': 123456,
    'event_id': 'dummy',
    'dummy': 'dummy',
}


def mock_coro(return_value=None, **kwargs):
    """Create mock coroutine function."""

    async def wrapped(*args, **kwargs):
        return return_value

    return Mock(wraps=wrapped, **kwargs)


@pytest.mark.asyncio()
async def test_order_events_agent(test_app, maxwell_dummy):
    """Test order_events_agent."""
    with patch(__name__ + '.dummy_agent') as mocked_agent:
        mocked_agent.send = mock_coro()

        async with order_events_agent.test_context() as agent:
            # await agent.put(maxwell_dummy)

            maxwell_dummy.data['payload'] = None
            try:
                await agent.put(maxwell_dummy)
                assert True
            except RuntimeError as e:
                assert str(e) == 'Event loop is closed'


@pytest.mark.asyncio()
async def test_dummy_agent(test_app):
    async with dummy_agent.test_context() as agent:
        event = await agent.put(dummy_payload)
        assert agent.results[event.message.offset] == dummy_payload
