from __future__ import annotations

from ast import literal_eval

import pytest
from deepdiff import DeepDiff
from manul.order_events.agents import order_was_overdue_agent
from manul.order_events.models import OrderWasOverdue


@pytest.mark.asyncio()
async def test_order_was_overdue_agent(
    test_app, maxwell_order_was_overdue, dwh_order_was_overdue,
):
    async with order_was_overdue_agent.test_context() as agent:
        order_events_data = maxwell_order_was_overdue.data

        data = {
            'order_event_id': order_events_data['id'],
            'event_id': order_events_data['event_id'],
            **literal_eval(order_events_data['payload']),
        }

        event = await agent.put(OrderWasOverdue(**data))
        result = agent.results[event.message.offset]
        diff = DeepDiff(result['fact_order_overdue'], dwh_order_was_overdue['fact_order_overdue'])
        assert diff == {}
