from __future__ import annotations

import asyncio
import json
import pathlib

import pytest
import resources
from manul.app import app
from manul.app import Maxwell


resources_path = pathlib.Path(resources.__file__).parent


# @pytest.fixture()
# def event_loop():
#     loop = asyncio.new_event_loop()
#     asyncio.set_event_loop(loop)

#     yield asyncio.get_event_loop()

@pytest.fixture()
def test_app(event_loop):
    app.finalize()
    app.conf.store = 'memory://'
    app.flow_control.resume()

    return app


@pytest.fixture
def maxwell_dummy() -> Maxwell:
    maxwell = Maxwell(
        database='dummy',
        table='dummy',
        type='dummy',
        ts=0,
        xid=None,
        commit=None,
        data={
            'id': 123456,
            'event_id': 'dummy',
            'order_id': 'dummy',
            'payload': {'dummy': 'dummy'},
            'event_name': 'dummy',
            'created_at': '2020-06-09 15:41:21',
        },
    )

    return maxwell


@pytest.fixture
def maxwell_order():
    with open(str(resources_path / 'orders.json'), 'r') as stream:
        records = json.load(stream)
        maxwell = Maxwell(**records[0])

        return maxwell


@pytest.fixture
def maxwell_order_was_created():
    records_path = str(resources_path / 'order_events' / 'order_was_created.json')
    with open(records_path, 'r') as stream:
        records = json.load(stream)
        maxwell = Maxwell(**records[0])

        return maxwell


@pytest.fixture
def maxwell_order_was_overdue():
    records_path = str(resources_path / 'order_events' / 'order_was_overdue.json')
    with open(records_path, 'r') as stream:
        records = json.load(stream)
        maxwell = Maxwell(**records[0])

        return maxwell


@pytest.fixture
def dwh_orders() -> dict:
    return {
        'dim_order': [
            {
                'order_id': '10bfabh1-6121-499a-afe2-9ddasd8993f0',
                'order_number': '54373493',
                'merchant_name': 'Exitoksa',
                'country_code': 'SA',
                'description': None,
                'payment_type': 'PAY_BY_LATER',
                'status': 'expired',
                'platform': None,
                'is_mobile': 0,
                'device_id': None,
                'created_at': '2020-10-23 03:10:32',
                'updated_at': '2021-10-05 04:53:24',
                'created_date_key': '20201023',
                'updated_date_key': '20211005',
            },
        ],
        'fact_order': [
            {
                'order_id': '10bfabh1-6121-499a-afe2-9ddasd8993f0',
                'created_date_key': '20201023',
                'updated_date_key': '20211005',
                'total_amount': 1190000,
                'total_currency': 'SAR',
            },
        ],
    }


@pytest.fixture
def dwh_order_was_created() -> dict:
    return {
        'fact_order_event': [
            {
                'order_event_id': 26452661,
                'event_id': '6d3fcf56-8433-4838-a386-7bb3d8593a56',
                'order_id': '04142f65-d4b4-4488-8756-de5cea01ede0',
                'event_name': 'Tamara\\Component\\Order\\Model\\Event\\OrderWasCreated',
                'created_at': '2020-06-09 15:41:21',
                'created_date_key': '20200609',
            },
        ],
        'dim_order_item': [
            {
                'order_event_id': 26452661,
                'sku': '193154357820',
                'name': 'Air-Zoom-Vomero-14 Plum Chalk/Metallic Gold-Infinite Gold',
                'order_id': '04142f65-d4b4-4488-8756-de5cea01ede0',
                'type': 'configurable',
                'image_url': 'https://stagingapp.nejree.com/media/catalog/product/cache/10f519365b01716ddb90abc57de5a837/7/e/7e6a4784.jpg',
                'reference_id': '312963',
                'created_date_key': '20200609',
                'merchant_id': '56ddf401-f597-4591-81ca-c1761ac5dc46',
                'merchant_name': 'Nejree',
            },
        ],
        'fact_order_item': [
            {
                'order_event_id': 26452661,
                'event_id': '6d3fcf56-8433-4838-a386-7bb3d8593a56',
                'order_id': '04142f65-d4b4-4488-8756-de5cea01ede0',
                'sku': '193154357820',
                'name': 'Air-Zoom-Vomero-14 Plum Chalk/Metallic Gold-Infinite Gold',
                'quantity': 1,
                'tax_amount': 19.43,
                'tax_currency': 'SAR',
                'unit_price': 388.57,
                'unit_currency': 'SAR',
                'total_amount': 408,
                'total_currency': 'SAR',
                'discount_amount': 0,
                'discount_currency': 'SAR',
                'created_date_key': '20200609',
                'merchant_id': '56ddf401-f597-4591-81ca-c1761ac5dc46',
                'merchant_name': 'Nejree',
            },
        ],
    }


@pytest.fixture
def dwh_order_was_overdue() -> dict:
    return {
        'fact_order_event': [
            {
                'order_event_id': 26452651,
                'event_id': '5feae707-6105-4578-b8b7-2d9b77e94310',
                'order_id': '3a169f33-e5b1-4967-b031-dddc5dcad1e5',
                'event_name': 'Tamara\\Component\\Order\\Model\\Event\\OrderWasOverdue',
                'created_at': '2022-04-09 03:23:28',
                'created_date_key': '20220409',
            },
        ],
        'fact_order_overdue': [
            {
                'order_event_id': 26452651,
                'event_id': '5feae707-6105-4578-b8b7-2d9b77e94310',
                'order_id': '3a169f33-e5b1-4967-b031-dddc5dcad1e5',
                'capture_id': 'e04c4349-25bc-4ae5-a364-4c05d599bdfd',
                'payment_id': 'e04c4349-25bc-4ae5-a364-4c05d599bdfd',
                'recorded_at': '2020-08-07T23:30:13+00:00',
                'recorded_date_key': '20200807',
                'late_fee_amount': 25,
                'late_fee_currency': 'SAR',
            },
        ],
    }
