<div id="top"></div>

---

<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
  </a>
  <h1 align="center">Stream processing pipeline</h1>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#data-warehouse-design">Data Warehouse Design</a></li>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#built-with">Built With</a></li>
    <li><a href="#getting-started">Getting Started</a></li>
    <li><a href="#run-project-locally">Run project locally</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#reporting">Reporting</a></li>
    <li><a href="#code-quality">Code Quality</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

## About The Project

![img](./assets/images/streaming-pipeline.png)

Manul project is a stream processing pipeline, that ingests data from MySQL database and sends them to Kafka. Then, Faust application will consume data from Kafka topics, and transformed by `petl` before loading to data warehouse.

Inside the project, Maxwell's daemon, an application that reads MySQL binlogs and writes row updates as JSON to Kafka. Maxwell has low operational overhead, requiring nothing but mysql and a place to write to. Its common use cases include ETL. Maxwell will send data to seperated topics like format: `maxwell[_%database][_%table]`. In this case, two topics from Tamara database, that Kafka will consume, are `maxwell_tarama_orders` and `maxwell_tamara_order_events`.

Faust is a stream processing library, porting the ideas from Kafka Streams to Python. Manul project uses Faust to define stream processors (agents), topics, channels, web views, CLI commands and more... Faust uses models to describe the fields of data structures used as keys and values in messages. They’re defined using a NamedTuple-like syntax:

```python
class Point(Record, serializer='json'):
    x: int
    y: int
```

Each records is a model of the dictionary type, having keys and values of a certain type. When using JSON as the serialization format, we have data like `{"x": 10, "y": 100}`. So we use this method to send serialized data to ETL tool to handle for next step.

For ETL tool, we use `petl`, a general purpose Python package for extracting, transforming and loading tables of data. `petl` uses simple python functions for providing a rows and columns abstraction for reading and writing data from files, databases, and other sources. To write data to databases, the project uses `SQLAlchemy` and `mysqlclient` to create connection.

Finally, Manul project includes a data warehouse that uses a dimension table is one of the set of companion tables to a fact table. The fact table contains business facts (or measures), and foreign keys which refer to candidate keys (normally primary keys) in the dimension tables. Moreover, optimizing mysql database via indexing tables to push retrieve data faster. Some visualization tools (Superset, Tableau, Power BI...) can pull data from the data warehouse to make business decisions reports.

<p align="right">(<a href="#top">back to top</a>)</p>

## Data Warehouse Design

* Using Dim Fact tables
* Opitimizing database quering by indexing

![img](./assets/images/dwh-schema.png)

## Built With

Structuring project folder by [PyScaffold](https://pyscaffold.org/en/stable/), and using [conda](https://docs.conda.io/en/latest/) to control virtual environment.

This project uses these main libraries:

* [Faust](https://faust.readthedocs.io/)
* [petl](https://petl.readthedocs.io/)
* [maxwells-daemon](https://maxwells-daemon.io/)
* [kafka](https://kafka.apache.org/)
* [SQLAlchemy](https://www.sqlalchemy.org/)

<p align="right">(<a href="#top">back to top</a>)</p>

## Getting Started

Run `docker-compose.yml` to start project.

```sh
docker-compose up -d
```

This will take some time to wait for `zookeeper` and `kafka` stablely before streaming the data from mysql source.

## Run project locally

Create virtual environment by `conda`:

```sh
conda create -n tamara python=3.8
conda activate tamara
pip install -r requirements.txt
```

Manul application have a mode `MANUL_MODE=dryrun` to preview data without inserting to data warehouse.

Create the script to run project locally (example: `/path/to/tamara.sh`)

```sh
export PYTHONPATH=./src:$PYTHONPATH

/path/to/anaconda3/envs/tamara/bin/python ./src/manul/__main__.py "$@"
```

Run script with `dryrun` mode:

```sh
$(tamara) ➜  MANUL_MODE=dryrun /bin/bash /path/to/tamara.sh worker
```

You can check how many agents can start:

```sh
$(tamara) ➜  /bin/bash /path/to/tamara.sh agents
```

![img](./assets/images/view-agents.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

After we start the project, the database will have a schema `maxwell` to trigger maxwell-bootstrap for syncing bin-log correctly.
![img](./assets/images/maxwell-database.png)

Default, maxwell-daemon won't stream all database from the beginning. It just syncs from the current bin-log position.

So we need to insert a record to `maxwell.bootstrap` to re-sync data to kafka.
Using these queries in MySQL Workbench:

```sql
-- trigger streaming for full tables
insert into maxwell.bootstrap(database_name, table_name) value('tamara', 'orders');
insert into maxwell.bootstrap(database_name, table_name) value('tamara', 'order_events');

-- or you can put the where clause if you want to resync certain the records
insert into maxwell.bootstrap(database_name, table_name, where_clause) value('tamara', 'orders', 'created_at <= "2020-06-01 00:00:00"');
```

Guildeline for maxwell-daemon bootstrapping here: https://maxwells-daemon.io/bootstrapping/

When receiving a new record with `dryrun` mode:

```sql
-- debug
insert into tamara.orders(id_order, merchant_name, order_number, country_code, description, payment_type, total_amount, total_currency, status, platform, is_mobile, device_id, created_at, updated_at)
select 'zzzcdadc-313f-46a7-822e-2e9ddfec0zzz' as id_order, merchant_name, order_number, country_code, description, payment_type, total_amount, total_currency, status, platform, is_mobile, device_id, created_at, updated_at
from tamara.orders
where id_order = 'fffcdadc-313f-46a7-822e-2e9ddfec07b8'
;
```

![img](assets/images/rsync-new-record.png)

## Reporting

You can get the original queries under `./scripts/`

1. Top 10 most purchased items by day, month, quarter, year.
(`./scripts/01_top10_most_purchased_items.sql`)
![img](./assets/images/01.png)

2. Top 10 items that contributed most to the late fee.
(`./scripts/02_top10_items_contributing_most_to_late_fee.sql`)
![img](./assets/images/02.png)

3. Top 10 merchants who have most new order value by day, month, quarter, year
(`./scripts/03_top10_merchants_having_most_new_order.sql`)
![img](./assets/images/03.png)

4. Top 10 merchants who have most canceled order value by day, month, quarter, year
(`./scripts/04_top10_merchants_having_most_canceled_order.sql`)
![img](./assets/images/04.png)

5. Total late fee amount collected by day, month, quarter, year (example: monthly report)
(`./scripts/05_total_late_fee_amount_collected.sql`)
![img](./assets/images/05.png)

## Code Quality

This project uses `tox` to automate and standardize testing in Python.

1. Using pytest for unittest and integration testing.
2. Config `.pre-commit-config.yaml` for lint checking:
   * pre-commit-hooks
   * mypy
   * autopep8
   * flake8
   * [Optional] pyupgrade

Testing this project:

```sh
tox -e py38,lint
```

![img](assets/images/code-quality-py38.png)
![img](assets/images/code-quality.png)

`pytest-asyncio` is an Apache2 licensed library, written in Python, for testing asyncio code with pytest.

asyncio code is usually written in the form of coroutines, which makes it slightly more difficult to test using normal testing tools.

## Roadmap

* [x] Code Quality
* [x] Gilab CI/CD deployment
* [ ] Add Changelog
* [ ] Partitioning Data Warehouse
* [ ] Strategies for data recovery in kafka and stream processor
* [ ] Cloud native (K8s)

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

Victor Pham - phamtanvinh.me@gmail.com

<p align="right">(<a href="#top">back to top</a>)</p>

## Acknowledgments

* [Introducing Faust](https://faust.readthedocs.io/en/latest/introduction.html)

* [Stream processing with Python Faust: Part I – General Concepts](https://www.8mincode.com/posts/how-to-stream-data-with-kafka-and-faust-general-concepts/)

* [Stream processing with Python Faust: Part II – Streaming pipeline](https://www.8mincode.com/posts/how-to-stream-data-with-kafka-and-faust-streaming-pipeline/)

* [petl - Extract, Transform and Load](https://petl.readthedocs.io/en/stable/)
![simple-etl-pipeline](https://petl.readthedocs.io/en/stable/_images/petl-architecture.png)

* [ GitLab CI/CD workflow](https://docs.gitlab.com/ee/ci/introduction/)
![simple-workflow](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)

<p align="right">(<a href="#top">back to top</a>)</p>
