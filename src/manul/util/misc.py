from __future__ import annotations

import dateutil.parser


def to_date_key(datetime_str: str) -> str:
    """Convert datetime string to datekey string."""
    _date = dateutil.parser.parse(datetime_str)

    return _date.strftime('%Y%m%d')
