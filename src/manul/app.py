from __future__ import annotations

from typing import Any
from typing import Optional

import faust
from manul.settings import KAFKA_BOOTSTRAP_SERVER


app = faust.App(
    'tamara',
    version=1,
    autodiscover=['manul.orders', 'manul.order_events'],
    origin='manul',
    broker=KAFKA_BOOTSTRAP_SERVER,
)


class Maxwell(faust.Record):
    database: str
    table: str
    type: str
    ts: int
    xid: Optional[int]
    commit: Optional[bool]
    data: Any

    def convert_data(self, ModelType: faust.Record) -> faust.Record:
        """Convert data to model type."""
        result = None
        if self.type in ('bootstrap-complete', 'bootstrap-start'):
            pass
        elif isinstance(self.data, dict):
            return ModelType(**self.data)

        return result
