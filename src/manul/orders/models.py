from __future__ import annotations

import faust


class Orders(faust.Record):
    id_order: str
    merchant_name: str
    order_number: str
    country_code: str
    description: str
    payment_type: str
    total_amount: int
    total_currency: str
    status: str
    platform: str
    is_mobile: int
    device_id: str
    created_at: str
    updated_at: str
