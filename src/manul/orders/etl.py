from __future__ import annotations

import logging

import petl
from manul.connection import get_connection
from manul.settings import MANUL_MODE
from manul.util.misc import to_date_key

from .models import Orders


logger = logging.getLogger(__name__)

table_meta = {
    'dim_order': [
        'order_id',
        'order_number',
        'merchant_name',
        'country_code',
        'description',
        'payment_type',
        'status',
        'platform',
        'is_mobile',
        'device_id',
        'created_at',
        'updated_at',
        'created_date_key',
        'updated_date_key',
    ],
    'fact_order': [
        'order_id',
        'created_date_key',
        'updated_date_key',
        'total_amount',
        'total_currency',
    ],
}


def etl_orders(order: Orders) -> dict:
    """ETL order."""
    tbl_order = petl.fromdicts([order.asdict()])
    tbl_order = petl.rename(tbl_order, {'id_order': 'order_id'})
    tbl_order = petl.addfields(
        tbl_order,
        [
            ('created_date_key', lambda row: to_date_key(row['created_at'])),
            ('updated_date_key', lambda row: to_date_key(row['updated_at'])),
        ],
    )

    dim_order = tbl_order.cut(table_meta['dim_order'])
    fact_order = tbl_order.cut(table_meta['fact_order'])

    result = {}
    if MANUL_MODE == 'dryrun':
        result = {
            'dim_order': list(petl.dicts(dim_order)),
            'fact_order': list(petl.dicts(fact_order)),
        }
        logger.warning(f'[MODE {MANUL_MODE}]: {result}')

    else:
        with get_connection() as conn:
            logger.warning(f'insert order {order.id_order} into fact_order')
            petl.appenddb(fact_order, conn, 'fact_order')

            logger.warning(f'insert order {order.id_order} into dim_order')
            petl.appenddb(dim_order, conn, 'dim_order')

    return result
