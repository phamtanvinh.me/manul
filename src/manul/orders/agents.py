from __future__ import annotations

from multiprocessing.connection import wait

from manul.app import app
from manul.app import Maxwell

from .etl import etl_orders
from .models import Orders


topic_name = 'maxwell_tamara_orders'
orders_topic = app.topic(topic_name, value_type=Maxwell)


@app.agent(orders_topic)
async def orders_agent(maxwell_orders):
    """Orders agent."""

    async for maxwell in maxwell_orders:
        order = maxwell.convert_data(Orders)
        if order is None:
            continue

        result = etl_orders(order)

        yield result
