from __future__ import annotations

from ast import literal_eval

from manul.app import app
from manul.app import Maxwell

from .etl import etl_event_order
from .etl import etl_order_was_created
from .etl import etl_order_was_overdue
from .models import OrderEvents
from .models import OrderWasCreated
from .models import OrderWasOverdue


topic_name = 'maxwell_tamara_order_events'
order_events_topic = app.topic(topic_name, value_type=Maxwell)

order_was_created_topic = app.topic(
    'OrderWasCreated', value_type=OrderWasCreated, internal=True,
)

order_was_overdue_topic = app.topic(
    'OrderWasOverdue', value_type=OrderWasOverdue, internal=True,
)

event_topics = {
    r'Tamara\Component\Order\Model\Event\OrderWasCreated': order_was_created_topic,
    r'Tamara\Component\Order\Model\Event\OrderWasOverdue': order_was_overdue_topic,
}

dummy_topic = app.topic('dummy', internal=True)


@app.agent(order_events_topic)
async def order_events_agent(stream):
    """Order events agent."""
    async for maxwell in stream:
        order_event = maxwell.convert_data(OrderEvents)
        if order_event is None:
            continue
        etl_event_order(order_event)
        payload = {
            'order_event_id': order_event.id,
            'event_id': order_event.event_id,
        }

        event_name = order_event.event_name
        if event_name in event_topics:
            topic = event_topics[event_name]
            if isinstance(order_event.payload, dict):
                payload.update(order_event.payload)
            elif isinstance(order_event.payload, str):
                payload.update(literal_eval(order_event.payload))
        else:
            payload.update({'dummy': 'dummy'})
            topic = dummy_topic

        await topic.send(value=payload)

        yield payload


@app.agent(order_was_created_topic)
async def order_was_created_agent(stream):
    """Order was created agent."""
    async for payload in stream:
        result = etl_order_was_created(payload)

        yield result


@app.agent(order_was_overdue_topic)
async def order_was_overdue_agent(stream):
    """Order was overdue agent."""
    async for payload in stream:
        result = etl_order_was_overdue(payload)

        yield result


@app.agent(dummy_topic)
async def dummy_agent(stream):
    """Dummy agent."""
    async for payload in stream:
        yield payload
