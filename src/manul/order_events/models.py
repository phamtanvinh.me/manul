from __future__ import annotations

from typing import Any
from typing import List
from typing import Optional

import faust


class OrderEvents(faust.Record):
    id: int
    event_id: str
    order_id: str
    event_name: str
    payload: dict
    created_at: str


class Amount(faust.Record):
    amount: float
    currency: str


class OrderItem(faust.Record):
    sku: str
    name: str
    type: str
    quantity: int
    image_url: str
    tax_amount: Amount
    unit_price: Amount
    reference_id: str
    total_amount: Amount
    discount_amount: Amount

    def to_dict(self) -> dict:
        return {
            'sku': self.sku,
            'name': self.name,
            'type': self.type,
            'quantity': self.quantity,
            'image_url': self.image_url,
            'tax_amount': self.tax_amount.amount,
            'tax_currency': self.tax_amount.currency,
            'unit_price': self.unit_price.amount,
            'unit_currency': self.unit_price.currency,
            'reference_id': self.reference_id,
            'total_amount': self.total_amount.amount,
            'total_currency': self.total_amount.currency,
            'discount_amount': self.discount_amount.amount,
            'discount_currency': self.discount_amount.currency,
        }


class MerchantUrl(faust.Record):
    cancel: str
    failure: str
    success: str
    notification: str


class OrderWasCreated(faust.Record):
    order_event_id: int
    event_id: str
    items: List[OrderItem]
    locale: str
    status: str
    discount: dict
    order_id: str
    platform: str
    is_mobile: bool
    created_at: str
    tax_amount: Amount
    description: str
    merchant_id: str
    country_code: str
    merchant_url: MerchantUrl
    payment_type: str
    total_amount: Amount
    merchant_name: str
    risk_assessment: Any
    shipping_amount: Amount
    order_reference_id: int


class OrderWasOverdue(faust.Record):
    order_event_id: int
    event_id: str
    order_id: str
    capture_id: Optional[str]
    payment_id: str
    recorded_at: str
    late_fee_amount: Amount

    def to_dict(self) -> dict:
        return {
            'order_event_id': self.order_event_id,
            'event_id': self.event_id,
            'order_id': self.order_id,
            'capture_id': self.capture_id,
            'payment_id': self.payment_id,
            'recorded_at': self.recorded_at,
            'late_fee_amount': self.late_fee_amount.amount,
            'late_fee_currency': self.late_fee_amount.currency,
        }
