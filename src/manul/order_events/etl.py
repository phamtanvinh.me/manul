from __future__ import annotations

import logging

import petl
from manul.connection import get_connection
from manul.settings import MANUL_MODE
from manul.util.misc import to_date_key

from .models import OrderEvents
from .models import OrderWasCreated
from .models import OrderWasOverdue


logger = logging.getLogger(__name__)

table_meta = {
    'fact_order_event': [
        'order_event_id',
        'event_id',
        'order_id',
        'event_name',
        'created_at',
        'created_date_key',
    ],
    'dim_order_item': [
        'order_event_id',
        'sku',
        'name',
        'order_id',
        'type',
        'image_url',
        'reference_id',
        'created_date_key',
        'merchant_id',
        'merchant_name',
    ],
    'fact_order_item': [
        'order_event_id',
        'event_id',
        'order_id',
        'sku',
        'name',
        'quantity',
        'tax_amount',
        'tax_currency',
        'unit_price',
        'unit_currency',
        'total_amount',
        'total_currency',
        'discount_amount',
        'discount_currency',
        'created_date_key',
        'merchant_id',
        'merchant_name',
    ],
    'fact_order_overdue': [
        'order_event_id',
        'event_id',
        'order_id',
        'capture_id',
        'payment_id',
        'recorded_at',
        'recorded_date_key',
        'late_fee_amount',
        'late_fee_currency',
    ],
}


def etl_event_order(order_event: OrderEvents) -> dict:
    """ETL order event."""
    tbl_order_event = petl.fromdicts([order_event.asdict()])
    tbl_order_event = petl.rename(tbl_order_event, {'id': 'order_event_id'})
    tbl_order_event = petl.addfields(
        tbl_order_event,
        [('created_date_key', lambda row: to_date_key(row['created_at']))],
    )

    fact_order_event = petl.cut(tbl_order_event, table_meta['fact_order_event'])

    result = {}
    if MANUL_MODE == 'dryrun':
        result = {'fact_order_event': list(petl.dicts(fact_order_event))}
        logger.warning(f'[MODE {MANUL_MODE}]: {result}')

    else:
        with get_connection() as conn:
            logger.warning(
                f'insert order_event {order_event.event_id} into fact_order_event',
            )
            petl.appenddb(fact_order_event, conn, 'fact_order_event')

    return result


def etl_order_was_created(payload: OrderWasCreated) -> dict:
    """ETL order_was_created."""
    items = [item.to_dict() for item in payload.items]
    tbl_order_items = petl.fromdicts(items)
    tbl_order_items = petl.addfields(
        tbl_order_items,
        [
            ('created_date_key', lambda row: to_date_key(payload.created_at)),
            ('order_id', lambda row: payload.order_id),
            ('event_id', lambda row: payload.event_id),
            ('order_event_id', lambda row: payload.order_event_id),
            ('merchant_id', lambda row: payload.merchant_id),
            ('merchant_name', lambda row: payload.merchant_name),
        ],
    )

    dim_order_item = petl.cut(tbl_order_items, table_meta['dim_order_item'])
    fact_order_item = petl.cut(tbl_order_items, table_meta['fact_order_item'])

    result = {}
    if MANUL_MODE == 'dryrun':
        result = {
            'dim_order_item': list(petl.dicts(dim_order_item)),
            'fact_order_item': list(petl.dicts(fact_order_item)),
        }
        logger.warning(f'[MODE {MANUL_MODE}]: {result}')

    else:

        with get_connection() as conn:
            logger.warning(f'insert order_event {payload.event_id} into dim_order_item')
            petl.appenddb(dim_order_item, conn, 'dim_order_item')

            logger.warning(
                f'insert order_event {payload.event_id} into fact_order_item',
            )
            petl.appenddb(fact_order_item, conn, 'fact_order_item')

    return result


def etl_order_was_overdue(payload: OrderWasOverdue) -> dict:
    """ETL order_was_overdue."""
    tbl_order_was_overdue = petl.fromdicts([payload.to_dict()])
    tbl_order_was_overdue = petl.addfields(
        tbl_order_was_overdue,
        [('recorded_date_key', lambda row: to_date_key(row['recorded_at']))],
    )

    fact_order_was_overdue = petl.cut(
        tbl_order_was_overdue, table_meta['fact_order_overdue'],
    )

    result = {}
    if MANUL_MODE == 'dryrun':
        result = {
            'fact_order_overdue': list(petl.dicts(fact_order_was_overdue)),
        }
        logger.warning(f'[MODE {MANUL_MODE}]: {result}')

    else:
        with get_connection() as conn:
            logger.warning(
                f'insert order_event {payload.event_id} into fact_order_overdue',
            )
            petl.appenddb(fact_order_was_overdue, conn, 'fact_order_overdue')

    return result
