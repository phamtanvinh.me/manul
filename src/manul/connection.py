from __future__ import annotations

from manul.settings import dwh_profile
from sqlalchemy import create_engine
from sqlalchemy.engine import Connection
from sqlalchemy.engine import Engine


def get_engine(profile: dict) -> Engine:
    """Docstring"""
    conn_str = 'mysql+mysqldb://{user}:{password}@{host}:{port}/{database}?charset=utf8mb4'.format(
        **profile,
    )

    engine = create_engine(conn_str)

    return engine


def get_connection(profile: dict = dwh_profile) -> Connection:
    """Docstring"""
    engine = get_engine(profile)
    conn: Connection = None

    try:
        conn = engine.connect()
        conn.execute('SET SQL_MODE=ANSI_QUOTES')
    except Exception as e:
        print(e)

    return conn
