from __future__ import annotations

import os


DWH_HOST = os.getenv('DWH_HOST') or '127.0.0.1'
DWH_PORT = os.getenv('DWH_PORT') or 3306
DWH_USER = os.getenv('DWH_USER') or 'root'
DWH_PASSWORD = os.getenv('DWH_PASSWORD') or 'root'
DWH_DATABASE = os.getenv('DWH_DATABASE') or 'dwh'

BROKER_HOST = os.getenv('BROKER_HOST') or 'localhost'
BROKER_PORT = os.getenv('BROKER_PORT') or 9092

KAFKA_BOOTSTRAP_SERVER = f'kafka://{BROKER_HOST}:{BROKER_PORT}'

MANUL_MODE = os.getenv('MANUL_MODE')

dwh_profile = {
    'host': DWH_HOST,
    'port': DWH_PORT,
    'user': DWH_USER,
    'password': DWH_PASSWORD,
    'database': DWH_DATABASE,
}
