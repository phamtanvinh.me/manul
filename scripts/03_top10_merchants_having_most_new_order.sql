-- --------------------------------------------------------------------------------------
-- Top 10 merchants who have most new order value by day, month, quarter, year
-- --------------------------------------------------------------------------------------

use dwh;

with recursive cte_rank as (
    select 1 as rank_num
    union all
    select rank_num + 1 from cte_rank where rank_num < 10
-- --------------------------------------------------------------------------------------
-- Aggregation by day
-- --------------------------------------------------------------------------------------
), group_by_day as (
    select
        d_d.year,
        cast(concat(d_d.year, d_d.quarter) as unsigned) as year_quarter_number,
        d_d.year_month_number,
        d_oi.created_date_key as date_key,
        d_oi.merchant_name as object_name,
        round(sum(if(total_currency = exchange_from, f_oi.total_amount*exchange_rate, f_oi.total_amount)), 2) as total
    from dim_order_item d_oi
    inner join fact_order_item f_oi
        on f_oi.order_id = d_oi.order_id and f_oi.sku = d_oi.sku
    inner join dim_date d_d
        on d_d.date_key = d_oi.created_date_key
    inner join dim_exchange_rate d_er
        on d_er.date_key = d_d.date_key
    group by d_d.year, d_d.quarter, d_d.year_month_number, d_oi.created_date_key, d_oi.merchant_name

), rank_by_day as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(date_key) desc) as rank_num
    from group_by_day
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by month
-- --------------------------------------------------------------------------------------
), group_by_month as (
    select object_name, year, year_quarter_number, year_month_number, sum(total) as total
    from group_by_day
    group by object_name, year, year_quarter_number, year_month_number
), rank_by_month as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year_month_number) desc) as rank_num
    from group_by_month
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by quarter
-- --------------------------------------------------------------------------------------
), group_by_quarter as (
    select object_name, year, year_quarter_number, sum(total) as total
    from group_by_month
    group by object_name, year, year_quarter_number

), rank_by_quarter as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year_quarter_number) desc) as rank_num
    from group_by_quarter
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by year
-- --------------------------------------------------------------------------------------
), group_by_year as (
    select object_name, year, sum(total) as total
    from group_by_quarter
    group by object_name, year
), rank_by_year as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year) desc) as rank_num
    from group_by_year
    group by object_name
)
-- --------------------------------------------------------------------------------------
-- Reporting
-- --------------------------------------------------------------------------------------
select
    r.rank_num,
    r1.object_name as merchant_in_day, r1.total as most_new_orders_in_day,
    r2.object_name as merchant_in_month, r2.total as most_new_orders_in_month,
    r3.object_name as merchant_in_quarter, r3.total as most_new_orders_in_quarter,
    r4.object_name as merchant_in_year,  r4.total as most_new_orders_in_year
from cte_rank r
    left join rank_by_day r1 on r1.rank_num = r.rank_num
    left join rank_by_month r2 on r2.rank_num = r.rank_num
    left join rank_by_quarter r3 on r3.rank_num = r.rank_num
    left join rank_by_year r4 on r4.rank_num = r.rank_num
;
