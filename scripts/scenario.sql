-- check tables have been created
select * from tamara.orders;
select * from tamara.order_events;

-- trigger streaming for full tables
insert into maxwell.bootstrap(database_name, table_name) value('tamara', 'orders');
insert into maxwell.bootstrap(database_name, table_name) value('tamara', 'order_events');

-- debug
insert into tamara.orders(id_order, merchant_name, order_number, country_code, description, payment_type, total_amount, total_currency, status, platform, is_mobile, device_id, created_at, updated_at)
select 'f??cdadc-313f-46a7-822e-2e9ddfec07b8' as id_order, merchant_name, order_number, country_code, description, payment_type, total_amount, total_currency, status, platform, is_mobile, device_id, created_at, updated_at
from tamara.orders
where id_order = 'fffcdadc-313f-46a7-822e-2e9ddfec07b8'
;
