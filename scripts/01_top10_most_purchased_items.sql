-- --------------------------------------------------------------------------------------
-- Top 10 most purchased items by day, month, quarter, year.
-- Solution: a valid order will have a list of valid items.
-- In this case, the orders wouldn't have a status in canceled, declined, and expried.
-- In each items, we will get most total amount by day, month, quarter or year.
-- Finally, we will get the top 10 most purchased items.
-- --------------------------------------------------------------------------------------

use dwh;

with recursive cte_rank as (
    select 1 as rank_num
    union all
    select rank_num + 1 from cte_rank where rank_num < 10
-- --------------------------------------------------------------------------------------
-- Aggregation by day
-- --------------------------------------------------------------------------------------
), group_by_day as (
    select
        d_d.year,
        cast(concat(d_d.year, d_d.quarter) as unsigned) as year_quarter_number,
        d_d.year_month_number,
        f_oi.created_date_key as date_key,
        f_oi.name as object_name,
        count(d_o.order_id) as total
    from dwh.dim_order d_o
    inner join dwh.dim_order_item f_oi on d_o.order_id = f_oi.order_id
    inner join dwh.dim_date d_d on d_d.date_key = f_oi.created_date_key
    where 1 = 1
        -- ------------------------------------------------------------------------------
        -- Filter condition
        -- ------------------------------------------------------------------------------
        and d_o.status not in ('canceled', 'declined', 'expried')
        and d_o.is_current = 1
    group by d_d.year, d_d.quarter, d_d.year_month_number, f_oi.created_date_key, f_oi.name
), rank_by_day as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(date_key) desc) as rank_num
    from group_by_day
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by month
-- --------------------------------------------------------------------------------------
), group_by_month as (
    select object_name, year, year_quarter_number, year_month_number, sum(total) as total
    from group_by_day
    group by object_name, year, year_quarter_number, year_month_number
), rank_by_month as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year_month_number) desc) as rank_num
    from group_by_month
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by quarter
-- --------------------------------------------------------------------------------------
), group_by_quarter as (
    select object_name, year, year_quarter_number, sum(total) as total
    from group_by_month
    group by object_name, year, year_quarter_number

), rank_by_quarter as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year_quarter_number) desc) as rank_num
    from group_by_quarter
    group by object_name

-- --------------------------------------------------------------------------------------
-- Aggregation by year
-- --------------------------------------------------------------------------------------
), group_by_year as (
    select object_name, year, sum(total) as total
    from group_by_quarter
    group by object_name, year
), rank_by_year as (
    select max(total) as total, object_name,
    row_number() over(order by max(total) desc, max(year) desc) as rank_num
    from group_by_year
    group by object_name
)
-- --------------------------------------------------------------------------------------
-- Reporting
-- --------------------------------------------------------------------------------------
select
    r.rank_num,
    r1.object_name as most_purchased_item_in_day, r1.total as total_in_day,
    r2.object_name as most_purchased_item_in_month, r2.total as total_in_month,
    r3.object_name as most_purchased_item_in_quarter, r3.total as total_in_quarter,
    r4.object_name as most_purchased_item_in_year,  r4.total as total_in_year
from cte_rank r
    left join rank_by_day r1 on r1.rank_num = r.rank_num
    left join rank_by_month r2 on r2.rank_num = r.rank_num
    left join rank_by_quarter r3 on r3.rank_num = r.rank_num
    left join rank_by_year r4 on r4.rank_num = r.rank_num
;
