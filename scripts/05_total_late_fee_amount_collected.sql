-- --------------------------------------------------------------------------------------
-- Total late fee amount collected by day, month, quarter, year.
-- --------------------------------------------------------------------------------------

use dwh;

-- --------------------------------------------------------------------------------------
-- Aggregation by day
-- --------------------------------------------------------------------------------------
with collected_by_day as (
    select
        d_d.year,
        cast(concat(d_d.year, d_d.quarter) as unsigned) as year_quarter_number,
        d_d.year_month_number,
        d_d.date_key,
        round(sum(if(late_fee_currency = exchange_from, late_fee_amount*exchange_rate, late_fee_amount)), 2) as total_late_fee_amount
    from fact_order_overdue d_odd
    inner join dim_date d_d on d_d.date_key = d_odd.recorded_date_key
    inner join dim_exchange_rate d_er on d_er.date_key = d_d.date_key
    group by d_d.year, d_d.quarter, d_d.year_month_number, d_odd.recorded_date_key

-- --------------------------------------------------------------------------------------
-- Aggregation by month
-- --------------------------------------------------------------------------------------
), collected_by_month as (
    select
        year_month_number, sum(total_late_fee_amount) as total_late_fee_amount
    from collected_by_day
    group by year_month_number

-- --------------------------------------------------------------------------------------
-- Aggregation by quarter
-- --------------------------------------------------------------------------------------
),  collected_by_quarter as (
    select
        year_quarter_number, sum(total_late_fee_amount) as total_late_fee_amount
    from collected_by_day
    group by year_quarter_number

-- --------------------------------------------------------------------------------------
-- Aggregation by year
-- --------------------------------------------------------------------------------------
), collected_by_year as (
    select
        year, sum(total_late_fee_amount) as total_late_fee_amount
    from collected_by_day
    group by year
)
-- --------------------------------------------------------------------------------------
-- Reporting
-- Please, uncomment the following lines to see the result.
-- --------------------------------------------------------------------------------------
-- select date_key, total_late_fee_amount from collected_by_day
select year_month_number, total_late_fee_amount from collected_by_month
-- select year_quarter_number, total_late_fee_amount from collected_by_quarter
-- select year, total_late_fee_amount from collected_by_year
;
