-- TODO: Update current orders before inserting new records.
-- Use this method will trigger an error when inserting and updating with a tons of actions.

-- USE dwh;

-- DELIMITER $$

-- CREATE TRIGGER before_dim_order_insert
-- BEFORE INSERT
-- ON dwh.dim_order FOR EACH ROW
-- BEGIN

--     UPDATE dwh.dim_order
--     SET is_current = 0
--     WHERE order_id = new.order_id
--     ;

-- END $$

-- CREATE TRIGGER before_fact_order_insert
-- BEFORE INSERT
-- ON dwh.fact_order FOR EACH ROW
-- BEGIN

--     UPDATE dwh.fact_order
--     SET is_current = 0
--     WHERE order_id = new.order_id
--     ;

-- END $$


-- DELIMITER ;
