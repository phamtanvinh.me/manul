USE dwh;

-- small-numbers table
DROP TABLE IF EXISTS numbers_small;
CREATE TABLE numbers_small (number INT);
INSERT INTO numbers_small VALUES (0),(1),(2),(3),(4),(5),(6),(7),(8),(9);

-- main numbers table
DROP TABLE IF EXISTS numbers;
CREATE TABLE numbers (number BIGINT);
INSERT INTO numbers
SELECT thousands.number * 1000 + hundreds.number * 100 + tens.number * 10 + ones.number
  FROM numbers_small thousands, numbers_small hundreds, numbers_small tens, numbers_small ones
LIMIT 1000000;


-- populate it with days
INSERT INTO dim_date (sid, date)
SELECT number, DATE_ADD( '2010-01-01', INTERVAL number DAY )
  FROM numbers
  WHERE DATE_ADD( '2010-01-01', INTERVAL number DAY ) BETWEEN '2010-01-01' AND '2025-01-01'
  ORDER BY number;

-- fill in other rows
UPDATE dim_date
SET
  date_key          = DATE_FORMAT( date, '%Y%m%d' ),
  julian_date       = TO_DAYS(date),
  day_of_week       = IF(DATE_FORMAT( date, "%w" ) = 0,7,DATE_FORMAT( date, "%w" )),
  day_of_week_full  = DATE_FORMAT( date, "%W" ),
  day_of_week_short = DATE_FORMAT( date, "%a" ),
  weekend_text      = IF( DATE_FORMAT( date, "%W" ) IN ('Saturday','Sunday'), 'Weekend', 'Weekday'),
  isweekend         = IF( DATE_FORMAT( date, "%W" ) IN ('Saturday','Sunday'), 1, 0),
  isworkday         = IF( DATE_FORMAT( date, "%W" ) IN ('Saturday','Sunday'), 0, 1),
  month             = DATE_FORMAT( date, "%m"),
  month_full        = DATE_FORMAT( date, "%M"),
  month_short       = DATE_FORMAT( date, "%b"),
  first_date_of_month = date_add(date,interval -DAY(date)+1 DAY),
  last_date_of_month  = LAST_DAY(date),
  days_in_month     = DAY(LAST_DAY(date)),
  quarter           = QUARTER( date ),
  year              = DATE_FORMAT( date, "%Y" ),
  year_month_number = CONCAT(DATE_FORMAT(date, "%Y"),DATE_FORMAT(date, "%m")),
  year_week_number  = CONCAT(DATE_FORMAT(date, "%Y"),DATE_FORMAT(date, "%v")),
  day_of_month      = DATE_FORMAT( date, "%d" ),
  day_of_month_text = DATE_FORMAT( date, "%D" ),
  day_of_year       = DATE_FORMAT( date, "%j" ),
  week_starting_monday = DATE_FORMAT(date,'%v');

-- clean up temporary tables
DROP TABLE IF EXISTS numbers_small;
DROP TABLE IF EXISTS numbers;
