-- --------------------------------------------------------------------------------------
-- Top 10 items that contributed most to the late fee.
-- Solution: each items will have a weight of value in total order value
-- so when a order has a late fee, that will share this fee for its item by item's weight
-- --------------------------------------------------------------------------------------

use dwh;

-- --------------------------------------------------------------------------------------
-- Caculate total late fee by order
-- --------------------------------------------------------------------------------------
with total_late_fee as (
    select
        order_id,
        sum(if(late_fee_currency = exchange_from, late_fee_amount*exchange_rate, late_fee_amount)) as total_late_fee
    from fact_order_overdue f_ood
    inner join dim_exchange_rate d_er on f_ood.recorded_date_key = d_er.date_key
    group by order_id
-- --------------------------------------------------------------------------------------
-- Caculate weight value of item in order
-- --------------------------------------------------------------------------------------
), item_weight_in_order as (
    select
        order_id,
        name as item_name,
        total_amount/sum(total_amount) over(partition by order_id) as weight
    from fact_order_item
    where 1 = 1
        -- ------------------------------------------------------------------------------
        -- Filter condition
        -- NOTE: same sku, but different name. Eg: SA-12436 with 2 name: Lego City 8601,Lego City 8602
        -- No need to use exchange rate here
        -- ------------------------------------------------------------------------------
    group by order_id, name, total_amount
)
-- --------------------------------------------------------------------------------------
-- Reporting
-- --------------------------------------------------------------------------------------
select
    item_name,
    round(sum(weight*total_late_fee), 2) as total_late_fee_contributed_amount
from item_weight_in_order t1
inner join total_late_fee t2
    on t1.order_id = t2.order_id
group by item_name
order by total_late_fee_contributed_amount desc
limit 10
;
