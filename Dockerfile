FROM python:3.8.13

RUN apt-get update \
    && apt-get install -y --no-install-recommends apt-utils

ENV PIP_FORMAT=legacy
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

RUN apt-get install -y netcat && apt-get autoremove -y

RUN adduser --disabled-password --gecos '' manul

WORKDIR /home/manul

COPY ./src/manul /home/manul
COPY ./requirements.txt /home/manul/requirements.txt
COPY ./bin /home/manul/bin

RUN pip install -r requirements.txt

ENTRYPOINT ["./bin/wait-for-services.sh"]

CMD ["./bin/run.sh", "${WORKER_PORT}"]
